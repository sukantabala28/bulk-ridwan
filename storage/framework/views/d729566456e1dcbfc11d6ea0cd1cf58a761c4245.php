<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(config('app.name', 'Laravel')); ?></title>
    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TLNT7S');</script>
<script type="text/javascript">(function(o){var b="https://api.autopilothq.com/anywhere/",t="23556ef2086d413ca59b399cb5747679decfd69f94864c3b8fbe825622ff831f",a=window.AutopilotAnywhere={_runQueue:[],run:function(){this._runQueue.push(arguments);}},c=encodeURIComponent,s="SCRIPT",d=document,l=d.getElementsByTagName(s)[0],p="t="+c(d.title||"")+"&u="+c(d.location.href||"")+"&r="+c(d.referrer||""),j="text/javascript",z,y;if(!window.Autopilot) window.Autopilot=a;if(o.app) p="devmode=true&"+p;z=function(src,asy){var e=d.createElement(s);e.src=src;e.type=j;e.async=asy;l.parentNode.insertBefore(e,l);};if(!o.noaa){z(b+"aa/"+t+'?'+p,false)};y=function(){z(b+t+'?'+p,true);};if(window.attachEvent){window.attachEvent("onload",y);}else{window.addEventListener("load",y,false);}})({});</script>

<!-- End Google Tag Manager -->
<style type="text/css">
    .auth-container  input.check-toog.left-toog+label{
        padding-left: 50px;
    }
.auth-container input.check-toog.left-toog+label:before {

    left: 12px;
}
</style>
</head>


<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLNT7S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div id="app">
        <?php echo $__env->yieldContent('content'); ?>
    </div>
    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        Stripe.setPublishableKey('pk_test_jvLdTVd50vuBJ9oGUJTA1Ttt');
        $(function() {
            var $form = $('#payment-form');
            $form.find('.submit').click(function(event) {
                $form.find('.submit').prop('disabled', true);
                Stripe.card.createToken($form, stripeResponseHandler);
                return false;
            });
            function stripeResponseHandler(status, response) {
                var $form = $('#payment-form');
                if (response.error) { 
                    console.log(response.error.message);
                    $form.find('.payment-errors').text(response.error.message).show();
                    $form.find('.submit').prop('disabled', false); 
                } else { 
                    var token = response.id;
                    console.log(token);
                    $form.append($('<input type="hidden" name="stripeToken">').val(token));
                 $form.get(0).submit();
                }
            };
            $('input[name=period]').change(function(){
                var period = $(this).val()
                $('.levels').hide();
                $('.levels.'+period).show();
                
            });
            $('input[name=level]').change(function(){
                $('input[name=level]').parent().removeClass('active focus');
                $(this).parent().addClass('active focus');
            });
        });
    </script>
</body>
</html>
