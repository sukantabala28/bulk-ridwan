<?php $__env->startSection('content'); ?>


<div class="container-fluid app-body settings-page">

    <?php if(session('status')): ?>
        <div class="alert alert-success">
            <?php echo e(session('status')); ?>

        </div>
    <?php endif; ?>
            
	<h3>Settings 
	<a href="https://bufferapp.com/oauth2/authorize?client_id=<?php echo e(env('BUFFER_CLIENT_ID')); ?>&redirect_uri=<?php echo e(env('BUFFER_REDIRECT')); ?>&response_type=code" class="btn btn-primary pull-right">Reconnect to Buffer</a>


	</h3>
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">Account</div>
				<div class="panel-body">
					<div class="row">
						<form method="POST" id="update-user">
							<?php echo e(csrf_field()); ?>

							<input type="hidden" name="id" value="<?php echo e($user->id); ?>">
							<div class="col-sm-6">
							  <div class="form-group">
							  <i class="input-icon fa fa-user"></i>
									<input type="text" name="first_name" class="form-control" value="<?php echo e($user->first_name); ?>" placeholder="First Name">
							  </div>    
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" name="last_name" class="form-control nlp" value="<?php echo e($user->last_name); ?>" placeholder="Last Name">
								</div>   
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
								<i class="input-icon fa fa-envelope-o"></i>
									<input type="email" name="email" class="form-control" value="<?php echo e($user->email); ?>" placeholder="Email" autocomplete="false">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
								<i class="input-icon fa fa-lock"></i>

									<input type="password" class="form-control" name="Password" placeholder="Password" autocomplete="false">
								</div>
							</div>
							<div class="col-sm-12 text-center">
								<button type="submit" class="btn btn-default">Update</button>
							</div>
						</form>                    
					</div>
				</div>
			</div>



			<div class="panel panel-default">
				<div class="panel-heading">Subscription</div>
				<div class="panel-body">

					<form method="POST" id="upgrade-plan">
						<?php echo e(csrf_field()); ?>

					  	<div class="form-group">

					    	<label>Current Plan:</label> <?php echo e($user->subscriptions[0]->name); ?>

					  	</div>
					  	<div class="form-group">
					    	<label>Change to:</label>
						</div>
						<div class="row plans">




										<div class="col-sm-12">
											<div class="prices-button">
												<div class="btn-group-container">

												<div class="btn-group" data-toggle="buttons">
													<label class="btn btn-default active">
														<input type="radio" name="period" value="monthly" checked> Monthly
													</label>
													<label class="btn btn-default">
														<input type="radio" name="period" value="yearly"> Yearly
													</label>
												</div>

												</div>	
												<div class="row">
													<div class="col-md-12">											
												<div class="btn-group-container">


												<div class="btn-group levels monthly active" data-toggle="buttons">
												<?php $__currentLoopData = $plans_m; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<label class="btn btn-default <?php if($user->subscriptions[0]->stripe_plan == $plan->slug): ?> active <?php endif; ?>">
														<span><?php echo e($plan->name); ?></span> <?php echo e($plan->stripe_plan); ?>


														<input type="radio" name="id" value="<?php echo e($plan->slug); ?>|<?php echo e($plan->name); ?>" <?php if($user->subscriptions[0]->stripe_plan == $plan->slug): ?> checked <?php endif; ?>> 
													</label>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</div>
												<div class="btn-group levels yearly" data-toggle="buttons">
												<?php $__currentLoopData = $plans_y; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<label class="btn btn-default <?php if($user->subscriptions[0]->stripe_plan == $plan->slug): ?> active <?php endif; ?>">
														<span><?php echo e($plan->name); ?></span> 
														<input type="radio" name="id" value="<?php echo e($plan->slug); ?>|<?php echo e($plan->name); ?>" <?php if($user->subscriptions[0]->stripe_plan == $plan->slug): ?> checked <?php endif; ?>> 
														
													</label>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</div>
											

												</div>	
</div>
												</div>	
												<div class="coppare_link">
												<br>
												  <a target="_blank" href="https://bulk.ly/pricing/ ">Compare plans</a>		
												</div>
											</div>
										</div>




						</div>
						<br>
						<button type="submit" class="btn btn-default">Change Plan</button>
					  	<?php if($user->subscriptions[0]->ends_at): ?>
							<button type="button" class="btn btn-link btn-danger">Closed</button>
					  	<?php else: ?> 
							<button type="button" class="btn btn-link close-account">Close Account</button>
					  	<?php endif; ?>

					</form>
					
			
					<form id="close-account-from" action="/close-account" method="post">
						<?php echo e(csrf_field()); ?>

						<input type="hidden" name="user_id" value="<?php echo e($user->id); ?>">
						<input type="hidden" name="user_plan" value="<?php echo e($user->subscriptions[0]->name); ?>">
					</form>			


				</div>
			</div>




		</div>


		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">Billing</div>
				<div class="panel-body">
					<ul class="list-unstyled m-0">
						<?php $__currentLoopData = $cards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $card): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($key + 1); ?>. <?php echo e($card->brand); ?> :- <?php echo e($card->last4); ?></li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>
					<hr>
					<form action="" method="POST" id="payment-form">
						<div class="payment-errors alert alert-danger"></div>
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
						<div class="row">
							<div class="col-sm-8">
								<div class="form-group">
									<i class="input-icon fa fa-credit-card"></i>
									<input class="form-control" type="text" size="20" data-stripe="number" placeholder="Credit Card Number"> 
								 </div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<input class="form-control nlp" type="text" size="4" data-stripe="cvc" placeholder="CVC"> 
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
								<i class="input-icon fa fa-calendar"></i>
									<input class="form-control input-inline" type="text" size="2" data-stripe="exp_month" placeholder="MM"> <span> / </span>
									<input class="form-control input-inline lp15" type="text" size="4" data-stripe="exp_year" placeholder="MM"> 
								</div>
							</div>
							<div class="col-sm-12 text-center">
								<button type="submit" class="btn btn-default submit">Save Card</button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Time zone</div>
				<div class="panel-body">
					<?php
					$timezones = array();
					foreach(timezone_abbreviations_list() as $abbr => $timezone){
					   foreach($timezone as $val){
					        if(isset($val['timezone_id'])){
					            array_push($timezones, $val['timezone_id']);
					        }
					    }
					}
					
					$timezones = array_unique($timezones);
					sort($timezones);


					?>
					<form action="" method="POST" id="timezone-form">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
						<input type="hidden" name="user_id" value="<?php echo e($user->id); ?>">
							<div class="form-group">
								<select class="form-control  nlp" name="timezone">
									<?php foreach ($timezones as $key => $timezone): ?>
										<option <?php if($user->timezone == $timezone) { echo "selected"; } ?> value="<?php echo $timezone;  ?>"> <?php echo $timezone;  ?> </option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="col-sm-12 text-center">
								<button type="button" class="btn btn-default submit">Save</button>
							</div>

					</form>
				</div>
			</div>
	

		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">Invoice</div>
				<div class="panel-body">
					<table class="table b-00">
						<?php $__currentLoopData = $invoices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<?php
								$months = array('January', 'February', 'March', 'April', 'May', 'Jun', 'July', 'August', 'September', 'October', 'November', 'December');
								?>

								<td>
                                <div class="media">
                                  <div class="media-left">
                                  	<span class="montone left"><?php echo e(substr($months[$invoice->date()->month - 1], 0, 1)); ?></span>
                                  </div>
                                  <div class="media-body">
                                    <span class="montone"> <h4><?php echo e($invoice->date()->toFormattedDateString()); ?></h4></span>
                                    <p>Paid <?php echo e(str_replace('before', 'ago', $invoice->date()->diffForHumans(\Carbon\Carbon::now()))); ?></p>
                                  </div>
                                </div>
								</td>

								<td><?php echo e($invoice->total()); ?></td>
								<td><a href="/user/invoice/<?php echo e($invoice->id); ?>">Download</a></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>