



<?php $__env->startSection('content'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						<div class="list-group">
						  	<a class="list-group-item <?php if(\Request::route()->getName()=='admin'): ?> active <?php endif; ?>" href="/admin/">Overview</a>
						  	<a class="list-group-item active" href="/admin/manage-user/">Manage User</a>
						  	<a class="list-group-item <?php if(\Request::route()->getName()=='admin/membership-plan'): ?> active <?php endif; ?>" href="/admin/membership-plan">Membership Plan</a>
						</div>
					</div>
					<div class="col-md-9">

					<ul class="list-inline">
						<li>
							<a class="btn btn-primary" href="/admin/create-user"> Create Account</a>
						</li>
					</ul>

					<h3>Edit User</h3>
						<form action="" method="post">
						<input type="hidden" name="id" value="<?php echo e($user->id); ?>">
						<?php echo e(csrf_field()); ?>

						  <div class="form-group<?php echo e($errors->has('first_name') ? ' has-error' : ''); ?>">
						    <label for="FirstName">First Name</label>
						    <input type="text" class="form-control" id="FirstName" name="first_name" placeholder="First Name" value="<?php echo e($user->first_name); ?>">
								<?php if($errors->has('first_name')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('first_name')); ?></strong>
									</span>
								<?php endif; ?>
						  </div>
						  <div class="form-group<?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
						    <label for="LastName">Last Name</label>
						    <input type="text" class="form-control" id="LastName" name="last_name" placeholder="Last Name" value="<?php echo e($user->last_name); ?>">
								<?php if($errors->has('last_name')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('last_name')); ?></strong>
									</span>
								<?php endif; ?>
						  </div>

						  <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
						    <label for="Email">Email</label>
						    <input type="email" class="form-control" id="Email" name="email" placeholder="Email " value="<?php echo e($user->email); ?>">
								<?php if($errors->has('email')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('email')); ?></strong>
									</span>
								<?php endif; ?>
						  </div>
						  <button type="submit" class="btn btn-default">Submit</button>
						</form>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>