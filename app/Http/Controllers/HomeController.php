<?php

namespace Bulkly\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;

use Illuminate\Support\Facades\Auth;

use Bulkly\User;

use Bulkly\SocialAccounts;

use Bulkly\BufferPosting;

use GuzzleHttp\Exception\RequestException;

use GuzzleHttp\Exception\ClientException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // get campaign Cookie
        $campaign_cookie = \Cookie::get('campaign');
        if(!$campaign_cookie){
            $this->middleware('auth');
            $this->middleware('confirmed');
            $this->middleware('billing');
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $campaign_cookie = \Cookie::get('campaign');
        $campaign_email = \Cookie::get('campaign_email');
        if ($campaign_cookie && $campaign_email) {
            $campaign_user = User::where('email', $campaign_email)->first();
            if(!$campaign_user){
                try {
                    $client = new Client();
                    $response = $client->request('POST', 'https://api.bufferapp.com/1/oauth2/token.json', [
                        'form_params' => [
                            'client_id' => env('BUFFER_CLIENT_ID'),
                            'client_secret' => env('BUFFER_CLIENT_SECRET'),
                            'redirect_uri' => env('BUFFER_REDIRECT'),
                            'code' => $request->code,
                            'grant_type' => 'authorization_code',
                        ]
                    ]);
                    $buffer_token = json_decode($response->getBody());
                    $bufferuser = file_get_contents('https://api.bufferapp.com/1/user.json?access_token='.$buffer_token->access_token);
                    $bufferuser = json_decode($bufferuser);
                    $campaign_user = User::where('buffer_id', $bufferuser->id)->first();
                    if($campaign_user){
                        \Auth::login($campaign_user);
                    }

                } catch (Exception $e) {
                    dump($e->getMessage());
                }
            }

        }




        if(!Auth::check()){
            // get campaign Cookie
            $campaign_cookie = \Cookie::get('campaign');
            $campaign_email = \Cookie::get('campaign_email');
            if($campaign_cookie && $campaign_email){
                $campaign_user = User::where('email', $campaign_email)->first();
                if(!$campaign_user){

                    $user_meta = array(
                        'temp_user' => true,
                        'temp_subs' => false,
                    );

                    $campaign_user = new User;
                    $campaign_user->name = ' ';
                    $campaign_user->first_name = ' ';
                    $campaign_user->last_name = ' ';
                    $campaign_user->email = $campaign_email;
                    $campaign_user->password = bcrypt($campaign_email);
                    $campaign_user->varifide = 1;
                    $campaign_user->user_meta = serialize($user_meta);
                    $campaign_user->save();
                    // delete campaign Cookie
                    \Auth::login($campaign_user);
                } else {
                    \Auth::login($campaign_user);
                }
            }
        }


        // get auth user
        $user = User::find(Auth::id());
        $user_meta = unserialize($user->user_meta);
        if($user_meta['temp_subs']===true){

        } else {

        }




        if (  $user->buffer_token ) {
            $request->session()->put('buffer_token', $user->buffer_token);
        }

        if(!empty($request->code)) {

            if(!isset($bufferuser)){
                $client = new Client();
                try {
                    $response = $client->request('POST', 'https://api.bufferapp.com/1/oauth2/token.json', [
                        'form_params' => [
                            'client_id' => env('BUFFER_CLIENT_ID'),
                            'client_secret' => env('BUFFER_CLIENT_SECRET'),
                            'redirect_uri' => env('BUFFER_REDIRECT'),
                            'code' => $request->code,
                            'grant_type' => 'authorization_code',
                        ]
                    ]);
                    $buffer_token = json_decode($response->getBody());
                    $bufferuser = file_get_contents('https://api.bufferapp.com/1/user.json?access_token='.$buffer_token->access_token);
                    $bufferuser = json_decode($bufferuser);

                } catch (Exception $e) {
                    dd($e->getMessage());
                }
            }
            if(!empty($request->code)) {

                $user->buffer_id = $bufferuser->id;
                $user->timezone = $bufferuser->timezone;
                $user->buffer_token = $buffer_token->access_token;
                $user->save();

                $request->session()->put('buffer_token', $buffer_token->access_token);
                $profiles = file_get_contents('https://api.bufferapp.com/1/profiles.json?access_token='.$request->session()->get('buffer_token'));
                $profiles = json_decode($profiles);
                $newaccountids = array();

                foreach ($profiles as $key => $profile) {
                    $social_account = SocialAccounts::where('account_id', $profile->id)->where('user_id', $user->id)->first();
                    if(!$social_account){
                        $sa = new SocialAccounts;
                        $sa->user_id = $user->id;
                        $sa->buffer_id = $bufferuser->id;
                        $sa->buffer_token = $buffer_token->access_token;
                        $sa->account_id = $profile->id;
                        $sa->type = $profile->service;
                        $sa->name = $profile->formatted_username;
                        $sa->avatar = $profile->avatar_https;
                        $sa->post_sent = serialize(array());
                        $sa->save();
                    } else {

                        $social_ac = SocialAccounts::find($social_account->id);
                        $social_ac->buffer_token = $buffer_token->access_token;
                        $social_ac->avatar = $profile->avatar_https;
                        $social_ac->save();
                    }
                    array_push($newaccountids, $profile->id);
                }

                $oldaccount = SocialAccounts::where('user_id', $user->id)->get();
                foreach ($oldaccount as $key => $oldaccount) {
                    if(in_array($oldaccount->account_id, $newaccountids)){
                    } else {
                        $oldaccount->delete();
                    }
                }

                $SocialPostGroups = \Bulkly\SocialPostGroups::where('user_id', \Auth::id())->get();

                if($SocialPostGroups->count() == 0){
                    $user = \Bulkly\User::find(\Auth::id());
                    foreach ($user->socialaccounts as $key => $socialaccount) {
                        $client = new Client();
                        try {
                            $result = $client->request('GET', 'https://api.bufferapp.com/1/profiles/'.$socialaccount->account_id.'/updates/sent.json?count=100&access_token='.$socialaccount->buffer_token);
                            $json = $result->getBody();
                        } catch (ClientException $e) {
                            $json = $e->getResponse()->getBody();
                        } catch (RequestException $e) {
                            $json = $e->getResponse()->getBody();
                        }
                        $posts = (isset(json_decode($json)->updates) ? json_decode($json)->updates : false);
                        if($posts){
                            $group_name = 'Buffer Import – '.ucwords(str_replace('google', 'google+', $socialaccount->type)).' – '.$socialaccount->name;
                            $group = new \Bulkly\SocialPostGroups;
                            $group->name = $group_name;
                            $group->user_id = $user->id;
                            $group->type = 'upload';
                            $group->status = 0;
                            $group->target_acounts = serialize(array($socialaccount->id));
                            $group->save();
                            foreach ($posts as $key => $bpost) {
                                if($key < 100){
                                    $post = new \Bulkly\SocialPosts;
                                    $post->group_id = $group->id;
                                    $post->text = $bpost->text;
                                    $post->link = isset($bpost->media->link) ? $bpost->media->link : null;
                                    $post->image = isset($bpost->media->picture) ? $bpost->media->picture : null;
                                    $post->save();
                                }
                            }
                        }
                    }
                }

                $__email = explode('@', \Auth::user()->email);
                if(isset($__email[1]) && $__email[1] == 'bulk.ly'){
                    return redirect(route('start'));
                } else {
                    return redirect(route('home'));
                }

                if($user_meta['temp_subs']===true){
                    dd('temp');
                } else {
                    dd('verified');
                }

                $SocialPostGroupsNew = \Bulkly\SocialPostGroups::where('user_id', \Auth::id())->first();
                if ($SocialPostGroupsNew) {
                    //dd(\Auth::user());
//                    return redirect(route('content-pending', $SocialPostGroupsNew->id));
                    return redirect(route('start'));
                } else {
                    dump('No Post Found');
                    return redirect(route('start'));
                }




            }
        }

        $months = array('01','02','03','04','05','06','07','08','09','10','11','12');
        $frequency = array();
        foreach ($months as $key => $month) {
            $bufferposting = BufferPosting::where('user_id', Auth::id())->whereMonth('sent_at', '=', $month)->count();
            array_push($frequency, $bufferposting);
        }
        $activities = BufferPosting::where('user_id', Auth::id())->limit(5)->orderBy('sent_at', 'desc')->get();
        $services = \DB::table('buffer_postings')->select(\DB::raw('count(*) as count, account_service'))->groupBy('account_service')->where('user_id', Auth::id())->get();
        return view('home')->with('user', $user)->with('services', $services)->with('frequency', $frequency)->with('activities', $activities);




    }

}
