<?php

namespace Bulkly\Http\Controllers;

use Dompdf\Exception;
use Illuminate\Http\Request;

use Bulkly\User;
use Bulkly\Plan;
use Bulkly\SocialPostGroups;

use Laravel\Cashier\Cashier;

use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class SubscriptionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = User::find(Auth::id());
        if (empty($user->subscriptions[0])){
             return view('subscriptions.subscriptions')
        ->with('plans_m', Plan::where('type', 'Month')->orderBy('price')->get())
        ->with('plans_y', Plan::where('type', 'Year')->orderBy('price')->get());
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Cashier::useCurrency('usd', '$');
        $user = User::find(Auth::id());
        $stripeToken = $request->input('stripeToken');

        $subscription = $user->newSubscription(explode('|', $request->input('id'))[1], explode('|', $request->input('id'))[0])->create($stripeToken);
        if($subscription){
            try{
                $subps = explode('|', $request->input('id'));
                /*$client = new Client();
                $result = $client->request('POST', 'https://api2.autopilothq.com/v1/contact', [
                    'headers' => [
                        'autopilotapikey' => env('AUTOP'),
                        'Content-Type'     => 'application/json'
                    ],
                    'json' => [
                        'contact' => [
                            'FirstName' => $user->first_name,
                            'LastName' =>$user->last_name,
                            'Email' => $user->email,
                            'custom' => [
                                'string--Subscription--Plan' => $subps[0],
                                ],
                            '_autopilot_list' => '9ECC7B84-9EB3-43EB-8C08-72A20E2573EA'
                        ]
                    ] //  
                ]);*/
            } catch (\Exception $e) {
            }
        }
        if($request->group_id){

            $group = SocialPostGroups::find($request->group_id);
            $group->status = 1;
            $group->save();

            \Cookie::queue(\Cookie::forget('campaign'));
            \Cookie::queue(\Cookie::forget('campaign_email'));

            $user_meta = array(
                'temp_user' => false,
                'temp_subs' => false,
            );

            if($request->first_name){
                $user->first_name = $request->first_name;
            }

            if($request->last_name){
                $user->last_name = $request->last_name;
            }

            $user->user_meta = serialize($user_meta);
            $user->save();

            return redirect(route('content-active', $request->group_id));





        } else {
            return redirect()->action('HomeController@index');
        }


        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
