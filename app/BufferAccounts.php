<?php

namespace Bulkly;

use Illuminate\Database\Eloquent\Model;

class BufferAccounts extends Model
{
   protected $table = 'buffer_accounts';
}
